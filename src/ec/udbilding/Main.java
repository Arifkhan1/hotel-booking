package ec.udbilding;

import jdk.swing.interop.SwingInterOpUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) throws InvalidAnswerException {
        List<Room> roomList = new ArrayList<>();
        Room rum1 = new Room(5000, 1, false, "one bed Luxury");
        Room rum2 = new Room(10000, 2, false, "two beds Luxury");
        Room rum3 = new Room(7000, 3, false, "three bed Economy");
        Room rum4 = new Room(12000, 4, false, "four beds Sweet");
        roomList.add(rum1);
        roomList.add(rum2);
        roomList.add(rum3);
        roomList.add(rum4);
//*****************************
        List<Services> servicesList = new ArrayList<>();
        List<Services> sList = new ArrayList<>();

        Services breakFast = new Services(500, 1, false, "BreakFast ");
        Services dinner = new Services(1000, 2, false, "Dinner  ");
        Services clean = new Services(200, 3, false, "Cleaning  ");
        Services coldDrink = new Services(150, 4, false, "Cold Drink ");
        sList.add(breakFast);
        sList.add(dinner);
        sList.add(clean);
        sList.add(coldDrink);



        //***************************
        System.out.println("Booking System 3000 ");
        System.out.println("\n==================");
        boolean inuse = true;
        while (inuse) {
            int choice = selectInput();
            switch (choice) {
                case 1:
                    bookRooms(roomList);
                    break;
                case 2:
                    unBook(roomList);
                    break;

                case 3:
                    listOfRoom(roomList);
                    break;
                case 4:
                    addSServices(roomList, sList);
                    break;
                case 5:
                    totalRevenue(roomList, servicesList);
                    break;

                case 6:
                    System.out.println("...");
                case 7:
                    inuse = false;
                    System.out.println("Goodbye!");
                    break;
            }
        }

    }

    public static int selectInput() throws InvalidAnswerException {
        List<Integer> room = List.of(1, 2, 3, 4, 5, 6);
        System.out.println("1. Book an available room ");
        System.out.println("2. Checked out the reservations");
        System.out.println("3. List of all the room");
        System.out.println("4. Add/Remove Service to room");
        System.out.println("5. Total Revenue of Booked rooms");
        System.out.println("6. Exit ");
        System.out.println("Your choice: ");
        System.out.println("\n");
        int choice = scanner.nextInt();
        if (!room.contains(choice)) {
            throw new InvalidAnswerException("Not a valid Option Brother");
        } else {
            return choice;
        }
    }

    public static void bookRooms(List<Room> list) {
        System.out.println("Following Rooms Are Available");
        for (Room room : list) {
            if (!room.isBooked()) {
                System.out.println(room.getNumber() + ". " + room.getName() + " = " + room.getPrice() + " SEK");
            }
        }
        System.out.println("\n-----------------------------");
        System.out.println("which room would you like to book?");
        int choice = scanner.nextInt();
        scanner.nextLine();

        for (Room room : list) {
            if (room.getNumber() == choice) {
                room.setBooked(true);

                System.out.println("*****************************");
                System.out.println("You have booked " + room.getNumber());
                System.out.println("*****************************");

            }

        }
    }

    public static void unBook(List<Room> list) {
        for (Room room : list) {
            if (room.isBooked()) {
                System.out.println("_______________________________________________________________");
                System.out.println(room.getNumber() + ". " + room.getName() + " Booked ");
            }
        }

        System.out.println("Which room do you want to Check out");
        int checkout = scanner.nextInt();
        for (Room room : list) {
            if (room.getNumber() == checkout) {
                room.setBooked(false);
                System.out.println("You have check out room no " + checkout);

            }
        }

    }


    public static void listOfRoom(List<Room> list) {
        System.out.println("Book and Unbooked Room list");
        for (Room room : list) {
            if (room.isBooked()) {
                System.out.println("_______________________________________________________________");
                System.out.println(room.getNumber() + ". " + room.getName() + " Booked ");
                System.out.println(room.getServicesList());
                System.out.println("_______________________________________________________________");
            } else System.out.println(room.getNumber() + ". " + room.getName() + " Available");
        }
        System.out.println("===============================================");
    }


    public static void addSServices(List<Room> list, List<Services> sList) {

        System.out.println("Select a Booked Room to handle the Room Services");
        System.out.println("\n-------------------------------------------------");


        for (Room room : list) {
            if (room.isBooked()) {
                System.out.println(room.getNumber() + ". " + room.getName());
            }
        }
        int selectRoom = scanner.nextInt();
        for (Room room : list) {
            if (room.getNumber() == selectRoom) {
                //  System.out.println("You have Selected Room "+ room.getNumber());
                List<Integer> serviceList = List.of(1, 2, 3);
                System.out.println("1. Add a room Service ");
                System.out.println("2. Remove room service");
                System.out.println("3. Return to Main Menu ");

                int opt = scanner.nextInt();
                switch (opt) {
                    case 1:
                        addServices(room, serviceList, opt, sList);
                        break;
                    case 2:
                        removeServices(room, serviceList);
                        break;
                    case 3:


                }

            }
        }
    }

    public static void addServices(Room room, List<Integer> serviceList, int opt, List<Services> sList) {

boolean x= true;
while (x){
        if (room.isBooked()) {
            System.out.println(room.getNumber() + ". " + room.getName());
        }
        System.out.println("The current room Services" + room.getServicesList());
        System.out.println("What do you want");
        if (serviceList.contains(opt)) {
            for (Services s : sList) {
                System.out.println(s.getNumber1() + ". " + s.getName1() + s.getPrice1() + " SEK");
            }
            System.out.println("\n-----------------------------------------------------");
            System.out.println("PLease Add the service ");
            System.out.println("--------------------------------------------------------");
            int opt2 = scanner.nextInt();

            int choice = opt2;

            switch (choice) {
                case 1:
                    room.getServicesList().add(new Services(500, 1, false, "BreakFast "));
                    System.out.println("BreakFast");
                    break;

                case 2:
                    room.getServicesList().add(new Services(1000, 2, false, "Dinner  "));
                    System.out.println(room.getNumber() + room.getName());
                    break;
                case 3:
                    room.getServicesList().add(new Services(200, 3, false, "Cleaning   "));
                    System.out.println("Cleaning");
                    break;
                case 4:
                    room.getServicesList().add(new Services(150, 4, false, "Cold Drink"));
                    System.out.println("Cold Drinks");

                    break;
                case 5:
           x=false;
                    System.out.println("_________________________________");
                    break;
            }
            }
        }
    }


    public static void removeServices(Room room, List<Integer> serviceList) {//{0:apple, orange, banana} 2-1=1
        System.out.println("The current room Services" + room.getServicesList());
        System.out.println("What Service do you want remove");

        int remove = scanner.nextInt();
        for (int i = 1; i < serviceList.size(); i++) {
            if (remove == serviceList.indexOf(serviceList.get(i))) {
                room.getServicesList().remove(i - 1);
            }

        }

    }


    public static void totalRevenue(List<Room> list,List<Services> servicesList) {

        for (Room room : list) {
            if (room.isBooked()) {
                System.out.println("_______________________________________________________________");
                System.out.println(room.getNumber() + ". " + room.getName() );
                System.out.println("Room Base Price " + room.getPrice());
                int revenue=0;
                for (Services serv:room.getServicesList()) {


                    revenue+=serv.getPrice1();
                    System.out.println(serv.getName1()+":"+serv.getPrice1());
                }
                revenue+= room.getPrice();
                System.out.println("  Total Price of the Room   "+revenue);



                }

            }
            int Trevenue = 0;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isBooked())
                    Trevenue += list.get(i).getTotalPrice();
            }
            System.out.println("_______________________________________________________________");
            System.out.println("The Total Revenue  of the Hotel =" + Trevenue);
            System.out.println("_______________________________________________________________");
        }

    }























