package ec.udbilding;

import java.util.ArrayList;
import java.util.List;

public  class Room {
    int price;
    int number;
    boolean booked = true;
    String name;
    List<Services> servicesList;


    public Room(int price, int number, boolean booked, String name) {
        this.price = price;
        this.booked = booked;
        this.name = name;
        this.number = number;
        this.servicesList = new ArrayList<>();
    }

    public int getPrice() {
        return price;
    }

    public boolean isBooked() {
        return booked;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public List<Services> getServicesList() {
        return servicesList;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    public String toString() {
        return name;
    }
    public  int getTotalPrice(){
        int revenue=0;
        for (int i = 0; i < servicesList.size(); i++) {
            revenue+=servicesList.get(i).getPrice1();
        }
        revenue+=getPrice();
        return revenue;
    }

}

